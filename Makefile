
all:

.PHONY: check trial pep8 clean distclean realclean

check: trial

trial: all
	trial test/test_jwz.py

pep8: all
	pep8 jwzthreading.py test/test_jwz.py

clean:
	-rm -rf _trial_temp
	find -name '*.pyc' -print0 | xargs -0 rm -f

distclean: clean

realclean: distclean
